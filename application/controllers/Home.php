<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data['page']='Home';
        $data['page_id']='home';
        $this->load->model('artikel_m');
        $data['artikel'] = $this->artikel_m->get_limit(5);
        $data['popular'] = $this->artikel_m->get_limit(5, 'hit_count');
        $data['featured'] = $this->artikel_m->get_featured();
        // print_r($data['featured']);die();
        $this->load->view('atas',$data);
        $this->load->view('tampil_utama');
        $this->load->view('bawah');
    }

    public function signout()
    {
        $this->session->unset_userdata('credit');
        redirect('home');
    }

    public function sessdest()
    {
        $this->session->sess_destroy();
        redirect('home');
    }

	public function notfound404()
	{
        $data['page']='Halaman Tidak Ditemukan [404]';
        $data['page_id']='404';
        $this->load->view('atas',$data);
        $this->load->view('tampil_eror');
        $this->load->view('bawah');
	}

}
