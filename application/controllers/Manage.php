<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->validate();
        $this->load->library('grocery_CRUD');
        $this->ss = $this->session->userdata('credit');
    }

    private function validate()
    {
        if(!$this->session->userdata('credit')['validated']){
            redirect('home');
        }
    }

    public function index()
    {
        $data['page'] = 'Dashboard';
        $data['page_id'] = 'dashboard';
        $this->load->view('admin/v_head',$data);
        $this->load->view('admin/v_dashboard');
        $this->load->view('admin/v_foot');
    }
    
    public function _manage($output = null)
    {
        $this->load->view('admin/v_head',$output);
        $this->load->view('admin/v_manage');
        $this->load->view('admin/v_foot');
    }

    public function article()
    {
        $page="Artikel";
        $page_id="artikel";
        $crud = new grocery_CRUD();
        $crud->set_subject('Artikel');
        $crud->unset_print();
        $crud->unset_export();
        $crud->required_fields('title', 'content', 'status', 'image', 'kategori_artikel_id', 'penulis_id');
        $crud->unset_fields('datetime_created', 'datetime_updated', 'hit_count');
        $crud->field_type('url', 'hidden', null);
        $crud->field_type('last_edited_by', 'hidden', $this->ss['username']);
        $crud->set_field_upload('image','assets/uploads/artikel');
        $crud->set_relation('kategori_artikel_id','kategori_artikel','nama_kategori', array('status'=>'aktif'));
        $crud->set_relation('penulis_id','penulis','nama_lengkap', array('status'=>'aktif'));
        $crud->callback_column('url',array($this,'visit_artikel'));
        $crud->callback_before_insert(array($this,'url_slug_insert_article'));
        $crud->callback_before_update(array($this,'url_slug_update_article'));
        $crud->display_as('kategori_artikel_id', 'Kategori');
        $crud->display_as('penulis_id', 'Penulis');
        $crud->display_as('datetime_updated', 'Tanggal Update');
        $crud->unset_columns('last_edited_by', 'datetime_created', 'image');
        $crud->set_table('artikel');
        $output = $crud->render();
        $output->page=$page;
        $output->page_id=$page_id;
        $this->_manage($output);
    }

    public function featuredarticle()
    {
        $page="Featured Artikel";
        $page_id="featuredarticle";
        $crud = new grocery_CRUD();
        $crud->set_subject('Featured Artikel');
        $crud->unset_print();
        $crud->unset_export();
        $check = $this->db->limit(1)->get('featured_artikel');
        if($check->num_rows()>0){
            $crud->unset_add();
        }
        $crud->set_relation('artikel_id','artikel','title', array('status'=>'aktif'));
        $crud->required_fields('artikel_id');
        $crud->display_as('artikel_id', 'Artikel');
        $crud->set_table('featured_artikel');
        $output = $crud->render();
        $output->page=$page;
        $output->page_id=$page_id;
        $this->_manage($output);
    }

    public function penulis()
    {
        $page="Penulis";
        $page_id="penulis";
        $crud = new grocery_CRUD();
        $crud->set_subject('Penulis');
        $crud->unset_print();
        $crud->unset_export();
        $crud->required_fields('nama_lengkap');
        $crud->set_field_upload('foto','assets/uploads/penulis');
        $crud->set_table('penulis');
        $output = $crud->render();
        $output->page=$page;
        $output->page_id=$page_id;
        $this->_manage($output);
    }

    public function imagestorage()
    {   
        //$rrr = 'asdasd.png';
        //$image_path = realpath(APPPATH . '../assets/uploads/imagestorage/sm/');
        //print_r($image_path.'/'.$rrr);die();
        $page="Image Storage";
        $page_id="imagestorage";
        $crud = new grocery_CRUD();
        $crud->set_subject('Image');
        $crud->unset_print();
        $crud->unset_export();
        $crud->unset_columns('title');
        $crud->unset_fields('title','datetime_created');
        $crud->required_fields('filename');
        $crud->field_type('uploaded_by', 'hidden', $this->ss['username']);
        $crud->set_field_upload('filename','assets/uploads/imagestorage');
        $crud->callback_column('filename',array($this,'smaller_list_img'));
        $crud->callback_after_upload(array($this,'create_thumbnails'));
        $crud->callback_before_delete(array($this,'delete_thumbnails'));
        $crud->set_table('imagestorage');
        $output = $crud->render();
        $output->page=$page;
        $output->page_id=$page_id;
        $this->_manage($output);
    }

    public function akun()
    {
        if($this->uri->segment(4)!=$this->ss['pk']){
            redirect(strtolower(__CLASS__).'/'.strtolower(__FUNCTION__).'/edit/'.$this->ss['pk']);
        }
        $page="Akun";
        $page_id="akun";
        $crud = new grocery_CRUD();
        $crud->set_subject('Akun');
        $crud->unset_print();
        $crud->unset_add();
        $crud->unset_read();
        $crud->unset_export();
        $crud->unset_list();
        $crud->unset_back_to_list();
        $crud->unset_edit_fields('prasswrord');
        $crud->add_fields('Password');
        $crud->callback_edit_field('Password',array($this,'pass'));
        $crud->display_as('rusernrame','Username');
        $crud->callback_before_update(array($this,'safe_username'));
        $crud->set_table('admin');
        try{
            $output = $crud->render();
        }
        catch(Exception $e){
            if($e->getCode() == 14)
            {
                redirect(strtolower(__CLASS__).'/'.strtolower(__FUNCTION__).'/edit/'.$this->ss['pk']);
            }
            else
            {
                show_error($e->getMessage());
            }
        }
        $output->page=$page;
        $output->page_id=$page_id;
        $this->_manage($output);
    }

    public function changepass()
    {
        $this->load->helper('security');
        if ($this->input->post('passbar')) {
            $pass = do_hash($this->input->post('passbar').UNIQPASS, 'md5');
            $data = array('prasswrord'=>$pass);
            $this->db->where('admin_id', $this->ss['pk']);
            $this->db->update('admin', $data);
            $this->session->set_flashdata('flsh_msg', 'Password Terganti');
            redirect('manage/akun/edit/'.$this->ss['pk']);
        }else{
            redirect(strtolower(__CLASS__).'/'.strtolower(__FUNCTION__).'/edit/'.$this->ss['pk']);
        }
    }



    /* Callback Functions */

    function pass($value, $primary_key)
    {
        return '<a href="#" style="color:black;text-decoration:none;" class="btn" data-toggle="modal" data-target="#changePass">Ganti Password</a>';
    }

    function visit_artikel($value, $row){
        return "<a href='".site_url('read/'.$row->url)."' target='_blank'>Baca</a>";
    }

    function slug_exist_article($url){
        $check = $this->db->get_where('artikel', array('url' => $url), 1);
        if($check->num_rows()==1){
            return true;
        }
    }

    function url_slug_insert_article($post_array)
    {
        $post_array['url'] = url_title($post_array['title'], 'dash', true);
        $i = 1;
        $baseSlug = $post_array['url'];
        while($this->slug_exist_article($post_array['url'])){
            $post_array['url'] = $baseSlug . "-" . $i++;
        }
        return $post_array; 
    }

    function url_slug_update_article($post_array, $primary_key)
    {
        $post_array['url'] = url_title($post_array['title'], 'dash', true);
        $i = 1;
        $baseSlug = $post_array['url'];
        while($this->slug_exist_article($post_array['url'])){
            $post_array['url'] = $baseSlug . "-" . $i++;
        }
        return $post_array; 
    }

    function create_thumbnails($uploader_response,$field_info, $files_to_upload)
    {
        $this->load->library('image_moo');
        $file_uploaded = $field_info->upload_path.'/'.$uploader_response[0]->name;
        $lg = $field_info->upload_path.'/lg/'.$uploader_response[0]->name;
        $md = $field_info->upload_path.'/md/'.$uploader_response[0]->name;
        $sm = $field_info->upload_path.'/sm/'.$uploader_response[0]->name;
        $xs = $field_info->upload_path.'/xs/'.$uploader_response[0]->name;
        $this->image_moo->load($file_uploaded)->resize(1024,758)->save($lg,true);
        $this->image_moo->load($file_uploaded)->resize(600,400)->save($md,true);
        $this->image_moo->load($file_uploaded)->resize(300,100)->save($sm,true);
        $this->image_moo->load($file_uploaded)->resize(150,50)->save($xs,true);
        return true;
    }

    public function smaller_list_img($value, $row)
    {
        return '<a href="'.base_url().'assets/uploads/imagestorage/'.$value.'" class="image-thumbnail"><img src="'.base_url().'assets/uploads/imagestorage/sm/'.$value.'" height="50px"></a>';
    }

    public function delete_thumbnails($primary_key)
    {
        $img = $this->db->where('id_imagestorage',$primary_key)->get('imagestorage')->row();
        if(empty($img))
        return false;
        $this->load->helper("file");
        $image_path = realpath(APPPATH . '../assets/uploads/imagestorage/');
        unlink($image_path."/lg/".$img->filename);
        unlink($image_path."/md/".$img->filename);
        unlink($image_path."/sm/".$img->filename);
        unlink($image_path."/xs/".$img->filename);
        unlink($image_path."/".$img->filename);
        return true;
    }

    function safe_username($post_array, $primary_key) {
            $nrusernrame = strtolower(preg_replace('/[^\da-z]/i', '_', $post_array['rusernrame']));
            $post_array['rusernrame'] = $nrusernrame;
        return $post_array;
    }
    
}