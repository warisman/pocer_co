<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('artikel_m');
        $this->load->library('pagination');
    }

    public function index()
    {
        if(!$this->uri->segment(3)&&$this->uri->segment(2)=='page'){redirect('post');}
        $data['page_id']='post';
        $config = array();
        $config['base_url'] = base_url() . 'post/page';
        $config['per_page'] = 10;
        $config['uri_segment'] = 3;
        $config['full_tag_open'] = '<ul class="pagination pagination-border pagination-lg">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = '&laquo&laquo';
        $config['last_link'] = '&raquo&raquo';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        if ($this->input->get('q')) {
            $q=$this->input->get('q');
            $data['page']='Hasil Pencarian "'.$q.'"';
            $data['artikel'] = $this->artikel_m->search($q, $config['per_page'], $page);
            $config['total_rows'] = $this->artikel_m->search_total($q);
        }else{
            $data['page']='Semua Rubrik';
            $data['artikel'] = $this->artikel_m->get_all($config['per_page'], $page);
        $config['total_rows'] = $this->artikel_m->total();
        }
        $this->pagination->initialize($config);
        $data['links'] = $this->pagination->create_links();
        $data['popular'] = $this->artikel_m->get_limit(5, 'hit_count');
        $this->load->view('atas',$data);
        $this->load->view('tampil_post');
        $this->load->view('bawah');
    }

}
