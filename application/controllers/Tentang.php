<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tentang extends CI_Controller {

    function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data['page']='Tentang Kami';
        $data['page_id']='tentang';
        $this->load->view('atas',$data);
        $this->load->view('tampil_tentang');
        $this->load->view('bawah');
    }

}
