<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Artikel_m extends CI_Model {
    
    function get_all($limit = null, $start = null, $order_by = 'artikel_id')
    {
        $this->db->limit($limit, $start);
        $query = $this->db->order_by($order_by,'desc')->join('penulis', 'penulis.penulis_id = artikel.penulis_id')->join('kategori_artikel', 'kategori_artikel.kategori_artikel_id = artikel.kategori_artikel_id')->where('artikel.status', 'aktif')->get('artikel');
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return $query->row();
    }
    
    function search($q, $limit = null, $start = null, $order_by = 'artikel_id')
    {
        $this->db->limit($limit, $start);
        $query = $this->db->order_by($order_by,'desc')->where('artikel.status', 'aktif')->join('penulis', 'penulis.penulis_id = artikel.penulis_id')->join('kategori_artikel', 'kategori_artikel.kategori_artikel_id = artikel.kategori_artikel_id')
            ->where("(`title` LIKE '%$q%' OR `content` LIKE '%$q%')")->get('artikel');
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return $query->row();
    }
    
    function get_limit($limit, $order_by = 'artikel_id')
    {
        $query = $this->db->order_by($order_by,'desc')->join('penulis', 'penulis.penulis_id = artikel.penulis_id')->join('kategori_artikel', 'kategori_artikel.kategori_artikel_id = artikel.kategori_artikel_id')->where('artikel.status', 'aktif')->limit($limit)->get('artikel');
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return $query->row();
    }

    function get_one_by($id, $where = 'artikel_id')
    {
        $query = $this->db->where($where, $id)->join('penulis', 'penulis.penulis_id = artikel.penulis_id')->join('kategori_artikel', 'kategori_artikel.kategori_artikel_id = artikel.kategori_artikel_id')->where('artikel.status', 'aktif')->limit(1)->get('artikel');
        return $query->row_array();
    }

    function get_featured()
    {
        $query = $this->db->limit(1)->join('artikel', 'artikel.artikel_id = featured_artikel.artikel_id')->where('artikel.status', 'aktif')->get('featured_artikel');
        return $query->row_array();
    }

    public function search_total($q) {
        return $this->db->like('title',$q)->or_like('content',$q)->where('artikel.status', 'aktif')->get('artikel')->num_rows();
    }

    public function total() {
        return $this->db->where('artikel.status', 'aktif')->count_all('artikel');
    }

}