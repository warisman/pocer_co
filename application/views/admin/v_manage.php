<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
                <?php if ($this->session->flashdata('flsh_msg')){ ?>
                    <div class="col-md-12" id="noTif">
                        <div class="panel bg-green-200">
                            <div class="panel-title bg-transparent">
                                <div class="panel-head color-white">Password berhasil di-update!</div>
                                <div class="panel-tools">
                                    <a href="#" id="closeMe" class="panel-close color-white"><i class="ion-close"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <div class="row">
                    <h2><?=$page?></h2>
                    <?=isset($output) ? $output : ''?>
                </div>
                <!-- /.row -->

                <?php if ($page_id=='akun') { ?>
                <div id="changePass" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Ganti Password</h4>
                            </div>
                            <div class="modal-body">
                                <form role="form" action="<?=base_url()?>manage/changepass" method="post">
                                    <div class="form-group">
                                        <input type="password" name="passbar" class="form-control" placeholder="Password baru...">
                                    </div>
                                    <input type="submit" class="btn btn-large" value="Submit">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
