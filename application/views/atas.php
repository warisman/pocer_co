<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <title><?=(isset($page)&&$page_id!='home')?$page." | ":""?><?=TITLE?> <?=(isset($page)&&$page_id=='home')?" | ".STITLE:""?></title>

    <meta name="description" content="<?=STITLE?>"/>
    <meta name="robots" content="index, follow"/>
    <link rel="canonical" href="<?=site_url()?>" />
    <meta property="og:locale" content="id_ID" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="<?=TITLE?> - <?=STITLE?>" />
    <meta property="og:description" content="<?=STITLE?>" />
    <meta property="og:url" content="<?=site_url()?>" />
    <meta property="og:site_name" content="<?=TITLE?>" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:description" content="<?=STITLE?>" />
    <meta name="twitter:title" content="<?=TITLE?> - <?=STITLE?>" />

    <link rel="shortcut icon" href="<?=base_url()?>assets/main/img/favicon.png" />

    <!-- CSS -->
    <link href="<?=base_url()?>assets/main/css/preload.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/main/css/vendors.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/main/css/style-orange3.css" rel="stylesheet" title="default">
    <link href="<?=base_url()?>assets/main/css/width-full.css" rel="stylesheet" title="default">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="<?=base_url()?>assets/main/js/html5shiv.min.js"></script>
        <script src="<?=base_url()?>assets/main/js/respond.min.js"></script>
    <![endif]-->
</head>

<!-- Preloader -->
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>

<body>

<div class="sb-slidebar sb-right sb-style-overlay">
<form role="form" method="get" action="<?=site_url('post')?>">
    <div class="input-group">
        <input type="text" class="form-control" name="q" placeholder="Pencarian...">
        <span class="input-group-btn">
            <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
        </span>
    </div><!-- /input-group -->
    <hr class="clear">
    <div class="slidebar-social-icons">
        <a href="#" class="social-icon-ar facebook"><i class="fa fa-facebook"></i></a>
        <a href="#" class="social-icon-ar twitter"><i class="fa fa-twitter"></i></a>
    </div>
</form>
</div> <!-- sb-slidebar sb-right -->

<div class="sb-site-container">
<div class="boxed">

<header id="header-full-top" class="hidden-xs header-full">
    <div class="container">
        <div class="header-full-title">
            <h1 class="animated fadeInRight"><a href="<?=site_url()?>"><img src="<?=base_url()?>assets/main/img/logo-pocerco_50.png"></a></h1>
            <p class="animated fadeInRight"><?=STITLE?></p>
        </div>
        <nav class="top-nav">
            <ul class="top-nav-social hidden-sm">
                <li><a href="#" class="animated fadeIn animation-delay-7 twitter"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#" class="animated fadeIn animation-delay-8 facebook"><i class="fa fa-facebook"></i></a></li>
            </ul>

            <div class="dropdown animated fadeInDown animation-delay-13">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-search"></i></a>
                <div class="dropdown-menu dropdown-menu-right dropdown-search-box animated fadeInUp">
                    <form role="form" method="get" action="<?=site_url('post')?>">
                        <div class="input-group">
                            <input type="text" class="form-control" name="q" placeholder="Pencarian...">
                            <span class="input-group-btn">
                                <button class="btn btn-ar btn-primary" type="button">Go!</button>
                            </span>
                        </div><!-- /input-group -->
                    </form>
                </div>
            </div> <!-- dropdown -->
        </nav>
    </div> <!-- container -->
</header> <!-- header-full -->
<nav class="navbar navbar-default navbar-header-full navbar-dark yamm navbar-static-top" role="navigation" id="header">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <i class="fa fa-bars"></i>
            </button>
            <a id="ar-brand" class="navbar-brand hidden-lg hidden-md hidden-sm" href="<?=site_url()?>"><img src="<?=base_url()?>assets/main/img/logo-pocerco_50.png"></a>
        </div> <!-- navbar-header -->

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="pull-right hidden-md hidden-lg hidden-sm">
            <a href="javascript:void(0);" class="sb-icon-navbar sb-toggle-right"><i class="fa fa-bars"></i></a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="<?=site_url()?>">Home</a></li>
                <li class="dropdown">
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">Ruang Baca</a>
                     <ul class="dropdown-menu dropdown-menu-left animated-2x animated fadeIn">
                        <li><a href="<?=site_url('category/peristiwa-literasi')?>">Peristiwa Literasi</a></li>
                        <li><a href="<?=site_url('category/sosok')?>">Sosok</a></li>
                        <li><a href="<?=site_url('category/resensi-buku')?>">Resensi Buku</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">Ruang Bicara</a>
                     <ul class="dropdown-menu dropdown-menu-left animated-2x animated fadeIn">
                        <li><a href="<?=site_url('category/di-balik-sampul')?>">Di Balik Sampul</a></li>
                        <li><a href="<?=site_url('category/wawancara')?>">Wawancara</a></li>
                        <li><a href="<?=site_url('category/teras-penerbit')?>">Teras Penerbit</a></li>
                        <li><a href="<?=site_url('category/cerita-kawan')?>">Cerita Kawan</a></li>
                    </ul>
                </li>
                <li><a href="<?=site_url('kontribusi')?>">Cara Berkontribusi</a></li>
                <li><a href="<?=site_url('tentang')?>">Tentang Kami</a></li>
             </ul>
        </div><!-- navbar-collapse -->
    </div><!-- container -->
</nav>