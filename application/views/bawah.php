<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<aside id="footer-widgets">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-4">
                        <div class="footer-widget margin-top-20">
                            <img class="img-responsive" src="<?=base_url()?>assets/main/img/logo-pocerco.png">
                        </div>
                    </div>
                    <div class="col-md-8 text-center">
                        <div class="footer-widget">
                            <p class="margin-top-20">
                                <a href="<?=site_url('category/peristiwa-literasi')?>"><span class="badge badge-primary badge-square">Peristiwa Literasi</span></a> 
                                <a href="<?=site_url('category/sosok')?>"><span class="badge badge-primary badge-square">Sosok</span></a> 
                                <a href="<?=site_url('category/resensi-buku')?>"><span class="badge badge-primary badge-square">Resensi Buku</span></a> 
                                <a href="<?=site_url('category/di-balik-sampul')?>"><span class="badge badge-primary badge-square">Di Balik Sampul</span></a> 
                                <a href="<?=site_url('category/wawancara')?>"><span class="badge badge-primary badge-square">Wawancara</span></a> 
                                <a href="<?=site_url('category/teras-penerbit')?>"><span class="badge badge-primary badge-square">Teras Penerbit</span></a> 
                                <a href="<?=site_url('category/cerita-kawan')?>"><span class="badge badge-primary badge-square">Cerita Kawan</span></a> 
                                <a href="<?=site_url('kontribusi')?>"><span class="badge badge-primary badge-square">Cara Berkontribusi</span></a> 
                                <a href="<?=site_url('penulis')?>"><span class="badge badge-primary badge-square">Penulis</span></a> 
                                <a href="<?=site_url('tentang/')?>"><span class="badge badge-primary badge-square">Tentang Kami</span></a> 
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- row -->
    </div> <!-- container -->
</aside> <!-- footer-widgets -->
<footer id="footer">
    <p>&copy; 2016 <a href="<?=site_url()?>"><?=TITLE?></a> | <?=STITLE?></p>
</footer>

</div> <!-- boxed -->
</div> <!-- sb-site -->

<div id="back-top">
    <a href="#header"><i class="fa fa-chevron-up"></i></a>
</div>

<!-- Scripts -->
<script src="<?=base_url()?>assets/main/js/vendors.js"></script>
<script src="<?=base_url()?>assets/main/js/styleswitcher.js"></script>
<script src="<?=base_url()?>assets/main/js/DropdownHover.js"></script>
<script src="<?=base_url()?>assets/main/js/app.js"></script>
<script src="<?=base_url()?>assets/main/js/holder.js"></script>

</body>
</html>
