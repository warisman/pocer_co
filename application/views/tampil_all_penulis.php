<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container margin-top">

    <div class="row">
        <h2 class="page-header no-margin-top"><?=$page?></h2>
        <?php if(isset($penulis)&&$penulis){ $a_i=5; foreach ($penulis as $penu) { ?>
        <div class="col-md-6">
            <section>
                <div class="panel panel-default  animated fadeIn animation-delay-<?=$a_i?>">
                    <div class="panel-body">
                        <h3 class="section-title no-margin-top"><?=$penu->nama_lengkap?></h3>
                        <div class="clearfix"></div>
                        <img src="<?=base_url()?>assets/uploads/penulis/<?=$penu->foto?>" alt="" class="alignleft imageborder">
                        <p class="no-margin-top"><?=$penu->deskripsi?></p>
                        <a href="#" class="social-icon-ar sm twitter animated fadeInDown"><i class="fa fa-twitter"></i></a>
                        <a href="#" class="social-icon-ar sm google-plus animated fadeInDown"><i class="fa fa-google-plus"></i></a>
                        <a href="#" class="social-icon-ar sm facebook animated fadeInDown"><i class="fa fa-facebook"></i></a>
                        <div class="clearfix"></div>
                        <a class="btn btn-ar btn-primary btn-lg" href="<?=site_url('penulis/'.$penu->username)?>">Lebih banyak tentang <?=$penu->nama_lengkap?></a>
                    </div>
                </div>
            </section>
        </div>
        <?php $a_i=$a_i+5; }}else{ ?>
            <p class="text-center">Tidak ada Penulis</p>
        <?php } ?>
    </div>

</div> <!-- container -->