<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container margin-top">
    <div class="col-md-8">
        <div class="row">

            <section>
                <h2 class="page-header no-margin-top"><?=$artikel['title']?></h2>
                <img src="<?=base_url()?>assets/uploads/artikel/<?=$artikel['image']?>" style="width: 100%;" class="img-responsive">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <i class="fa fa-clock-o"></i> <?=nice_date($artikel['datetime_updated'], 'd M Y')?>
                        <i class="fa fa-user"></i> <a href="<?=site_url('penulis/'.$artikel['username'])?>"><?=$artikel['nama_lengkap']?></a>
                        <i class="fa fa-folder-open"></i> <a href="<?=site_url('category/'.$artikel['url_kategori'])?>"><?=$artikel['nama_kategori']?></a>
                    </div>
                </div>
                <?=$artikel['content']?>
                <br>
                <?=$artikel['image_credit']!=''?'<em>Kredit Gambar : '.$artikel['image_credit'].'</em>':''?>
                <div class="panel panel-default margin-top-20">
                    <div class="panel-body">
                        <div class="col-md-12">
                            <img src="<?=base_url()?>assets/uploads/penulis/<?=$artikel['foto']?>" style="margin-right: 15px; width: 100px;" class="pull-left img-responsive imageborder">
                            <a href="<?=site_url('penulis/'.$artikel['username'])?>"><?=$artikel['nama_lengkap']?></a><br>
                            <?=$penulis['deskripsi']?>
                        </div>
                    </div>
                </div>
            </section>
            <section>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <span style="font-size: larger;">Bagikan : </span>
                        <a href="#" class="social-icon-ar sm no-margin-bottom facebook" rel="nofollow" target="_blank" onclick="share_me('https://www.facebook.com/sharer/sharer.php?u=<?=base_url(uri_string())?>&t=<?=$artikel['title']?>','facebook'); return false;"><i class="fa fa-facebook"></i></a>
                        <a href="#" class="social-icon-ar sm no-margin-bottom twitter" rel="nofollow" target="_blank" onclick="share_me('https://twitter.com/intent/tweet?text=<?=$artikel['title']?>&url=<?=base_url('r/'.$artikel['artikel_id'])?>&counturl=<?=base_url(uri_string())?>&related=pocer.co&via=pocer.co&hashtags=pocer.co','twitter'); return false;"><i class="fa fa-twitter"></i></a>
                        <a href="#" class="social-icon-ar sm no-margin-bottom google-plus" rel="nofollow" target="_blank" onclick="share_me('https://plus.google.com/share?url=<?=base_url(uri_string())?>','google'); return false;"><i class="fa fa-google-plus"></i></a>
                    </div>
                </div>
            </section>
            <section>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div id="disqus_thread"></div>
                        <script>
                        var disqus_config = function () {
                            this.page.url = '<?=base_url(uri_string())?>';
                            this.page.identifier = '<?=$artikel["artikel_id"]?>';
                        };
                        (function() {
                        var d = document, s = d.createElement('script');
                        s.src = '//pocer-co.disqus.com/embed.js';
                        s.setAttribute('data-timestamp', +new Date());
                        (d.head || d.body).appendChild(s);
                        })();
                        </script>
                        <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
                    </div>
                </div>
            </section>

        </div>
    </div> <!-- col-md-8 -->

    <div class="col-md-4">
        <h2 class="page-header no-margin-top">Terpopuler</h2>
        <?php if(isset($popular)&&$popular){ $a_p=5; foreach ($popular as $popu) { ?>
            <div class="home-news-box animated fadeInRight animation-delay-<?=$a_p?>">
                <h5 class="no-margin"><a href="<?=site_url('read/'.$popu->url)?>"><?=$popu->title?></a></h5>
                <small><a href="<?=site_url('penulis/'.$popu->username)?>"><?=$popu->nama_lengkap?></a> | <a href="<?=site_url('category/'.$popu->url_kategori)?>"><?=$popu->nama_kategori?></a> | <?=nice_date($popu->datetime_updated, 'd M Y')?></small>
                <img src="<?=base_url()?>assets/uploads/artikel/<?=$popu->image?>" class="img-post img-responsive">
            </div>
        <?php $a_p=$a_p+5; }}else{ ?>
            <p class="text-center">Tidak ada Artikel</p>
        <?php } ?>
    </div> <!-- col-md-4 -->

</div> <!-- container -->

<script type="text/javascript">
    var share_me = function(oUrl, oService) {
    var wnd;
    var w = 800;
    var h = 500;
    if (oService == "twitter") {
        w = 500;
        h = 300;
    }
    var left = (screen.width / 2) - (w / 2);
    var top = (screen.height / 2) - (h / 2);
    if (oService == "twitter") {
        wnd = window.open(oUrl, "share_window", "height=300,width=500,resizable=1,scrollbars=yes,top=" + top + ",left=" + left);
    } else {
        wnd = window.open(oUrl, "share_window", "height=500,width=800,resizable=1,scrollbars=yes,top=" + top + ",left=" + left);
    }
};
</script>