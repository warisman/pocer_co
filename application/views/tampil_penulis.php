<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<header class="services-header no-margin-bottom">
    <div class="primary-dark-div">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-push-4 col-md-7 col-md-push-5 col-sm-6 col-sm-push-6">
                    <div class="profile-header-text">
                        <h1><?=$penulis['nama_lengkap']?></h1>
                        <h2>Penulis</h2>
                    </div>
                </div>
            </div>
            <div class="profile-avatar-container">
                <img class="profile-avatar" src="<?=base_url()?>assets/uploads/penulis/<?=$penulis['foto']?>">
            </div>
        </div>
    </div>
</header>

<nav class="nav-profile">
    <div class="container">
        <div class="col-lg-8 col-lg-push-4 col-md-7 col-md-push-5 col-sm-6 col-sm-push-6">
            <ul class="profile-counters">
                <!-- <li><a href="#">14 <span>Posts</span></a></li> -->
                <!-- <li><a href="#">75 <span>Comments</span></a></li>
                <li><a href="#">148 <span>Tweets</span></a></li> -->
            </ul>
        </div>
    </div>
</nav>

<div class="container">
    <div class="clearfix"></div>
    <hr class="clear">
    <div class="row">
        <div class="col-md-5">
            <h2 class="right-line">Personal Information</h2>
            <section>
                <div class="panel panel-primary">
                    <div class="panel-heading"><?=$penulis['nama_lengkap']?></div>
                    <div class="panel-body">
                        <?=$penulis['deskripsi']?>
                        <hr>
                        <a href="#" class="social-icon-ar sm twitter animated fadeInDown"><i class="fa fa-twitter"></i></a>
                        <a href="#" class="social-icon-ar sm google-plus animated fadeInDown"><i class="fa fa-google-plus"></i></a>
                        <a href="#" class="social-icon-ar sm facebook animated fadeInDown"><i class="fa fa-facebook"></i></a>
                    </div>
                </div>
            </section>
        </div>
        <div class="col-md-7">
            <h2 class="right-line">Rubrik Oleh <?=$penulis['nama_lengkap']?></h2>
            <ul class="timeline-2">
            <?php if(isset($artikel)&&$artikel){ $a_i=5; foreach ($artikel as $arti) { ?>
                <li class="animated fadeIn animation-delay-<?=$a_i?>">
                    <time class="timeline-time hidden-xs" datetime=""><?=nice_date($arti->datetime_updated, 'Y-m-d')?> <span><?=nice_date($arti->datetime_updated, 'F')?></span></time>
                    <i class="timeline-2-point"></i>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h3 class="post-title"><a href="<?=site_url('read/'.$arti->url)?>" class="transicion"><?=$arti->title?></a></h3>
                            <div class="row">
                                <div class="col-lg-6">
                                    <img src="<?=base_url()?>assets/uploads/artikel/<?=$arti->image?>" class="img-post img-responsive">
                                </div>
                                <div class="col-lg-6 post-content">
                                    <p class="text-justify"><?=character_limiter(strip_tags($arti->content), 250)?></p>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer post-info-b">
                            <div class="row">
                                <div class="col-md-12">
                                    <span class="visible-xs"><i class="fa fa-clock-o"></i> <?=nice_date($arti->datetime_updated, 'd M Y')?></span> <i class="fa fa-user"></i> <a href="<?=site_url('penulis/'.$arti->username)?>"><?=$arti->nama_lengkap?></a> <i class="fa fa-folder-open"></i> <a href="<?=site_url('category/'.$arti->url_kategori)?>"><?=$arti->nama_kategori?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            <?php $a_i=$a_i+5; }}else{ ?>
                <p class="text-center">Tidak ada Artikel</p>
            <?php } ?>
            </ul>
        </div>
    </div>
</div>