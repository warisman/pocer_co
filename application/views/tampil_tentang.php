<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container margin-top">

    <div class="row">
        <div class="col-xs-12">
            <h1 class="right-line no-margin-top"><?=$page?></h1>
            <p class="slogan text-center animated bounceInLeft animation-delay-12">Po<span>Cer</span>.Co adalah rumah bagi yang <span>bergembira dengan membaca</span>;<br>Untuk <span>bertukar sapa</span> dan <span>wacana seputar literasi</span>.</p>
        </div>

        <div class="col-md-3">
            <div class="panel panel-default panel-card">
                <div class="panel-heading">
                    <img src="<?=base_url()?>assets/main/img/back.png" />
                </div>
                <div class="panel-figure">
                    <img class="img-responsive img-circle" src="<?=base_url()?>assets/main/img/eka.jpeg" />
                </div>
                <div class="panel-body text-center">
                    <h4 class="panel-header">W P Eka P</h4>
                    <small>Penanggung Jawab</small>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-default panel-card">
                <div class="panel-heading">
                    <img src="<?=base_url()?>assets/main/img/back.png" />
                </div>
                <div class="panel-figure">
                    <img class="img-responsive img-circle" src="<?=base_url()?>assets/main/img/kangcep.jpeg" />
                </div>
                <div class="panel-body text-center">
                    <h4 class="panel-header">Cep Subhan KM</h4>
                    <small>Pemimpin Redaksi</small>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-default panel-card">
                <div class="panel-heading">
                    <img src="<?=base_url()?>assets/main/img/back.png" />
                </div>
                <div class="panel-figure">
                    <img class="img-responsive img-circle" src="<?=base_url()?>assets/main/img/kangolih.jpeg" />
                </div>
                <div class="panel-body text-center">
                    <h4 class="panel-header">O Lihin</h4>
                    <small>Redaktur</small>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-default panel-card">
                <div class="panel-heading">
                    <img src="<?=base_url()?>assets/main/img/back.png" />
                </div>
                <div class="panel-figure">
                    <img class="img-responsive img-circle" src="<?=base_url()?>assets/main/img/imam.jpg" />
                </div>
                <div class="panel-body text-center">
                    <h4 class="panel-header">Imam</h4>
                    <small>Web Dev</small>
                </div>
            </div>
        </div>

    </div>

</div> <!-- container -->