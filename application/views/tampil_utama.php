<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container margin-top">
    <div class="col-md-8">
        <div class="row">

            <?php if(isset($featured)&&$featured){ ?>
            <div class="carousel-images margin-bottom">
                <div class="carousel-inner">
                    <div class="item active" style="height: 450px;">
                        <img src="<?=base_url()?>assets/uploads/artikel/<?=$featured['image']?>" style="height:100%;width:100%; object-position: center; object-fit: cover;">
                        <div class="carousel-caption carousel-caption-dark animated fadeInUpBig">
                            <h3><a href="<?=site_url('read/'.$featured['url'])?>"><?=$featured['title']?></a></h3>
                        </div>
                    </div>
                </div>
            </div> <!-- carousel -->
            <?php } ?>

            <h2 class="page-header no-margin-top">Terbaru</h2>
            <ul class="timeline-2">
            <?php if(isset($artikel)&&$artikel){ $a_i=5; foreach ($artikel as $arti) { ?>
                <li class="animated fadeIn animation-delay-<?=$a_i?>">
                    <time class="timeline-time hidden-xs" datetime=""><?=nice_date($arti->datetime_updated, 'Y-m-d')?> <span><?=nice_date($arti->datetime_updated, 'F')?></span></time>
                    <i class="timeline-2-point"></i>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h3 class="post-title"><a href="<?=site_url('read/'.$arti->url)?>" class="transicion"><?=$arti->title?></a></h3>
                            <div class="row">
                                <div class="col-lg-6">
                                    <img src="<?=base_url()?>assets/uploads/artikel/<?=$arti->image?>" class="img-post img-responsive">
                                </div>
                                <div class="col-lg-6 post-content">
                                    <p class="text-justify"><?=character_limiter(strip_tags($arti->content), 250)?></p>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer post-info-b">
                            <div class="row">
                                <div class="col-md-12">
                                    <span class="visible-xs"><i class="fa fa-clock-o"></i> <?=nice_date($arti->datetime_updated, 'd M Y')?></span> <i class="fa fa-user"></i> <a href="<?=site_url('penulis/'.$arti->username)?>"><?=$arti->nama_lengkap?></a> <i class="fa fa-folder-open"></i> <a href="<?=site_url('category/'.$arti->url_kategori)?>"><?=$arti->nama_kategori?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            <?php $a_i=$a_i+5; }}else{ ?>
                <p class="text-center">Tidak ada Artikel</p>
            <?php } ?>
            </ul>
            <hr>
            <h3 class="timeline-title"><a class="btn btn-ar btn-primary" href="<?=site_url('post')?>">Lihat Semua</a></h3>

        </div>
    </div> <!-- col-md-8 -->

    <div class="col-md-4">
        <h2 class="page-header no-margin-top">Terpopuler</h2>
        <?php if(isset($popular)&&$popular){ $a_p=5; foreach ($popular as $popu) { ?>
            <div class="home-news-box animated fadeInRight animation-delay-<?=$a_p?>">
                <h5 class="no-margin"><a href="<?=site_url('read/'.$popu->url)?>"><?=$popu->title?></a></h5>
                <small><a href="<?=site_url('penulis/'.$popu->username)?>"><?=$popu->nama_lengkap?></a> | <a href="<?=site_url('category/'.$popu->url_kategori)?>"><?=$popu->nama_kategori?></a> | <?=nice_date($popu->datetime_updated, 'd M Y')?></small>
                <img src="<?=base_url()?>assets/uploads/artikel/<?=$popu->image?>" class="img-post img-responsive">
            </div>
        <?php $a_p=$a_p+5; }}else{ ?>
            <p class="text-center">Tidak ada Artikel</p>
        <?php } ?>
        <hr>
    </div> <!-- col-md-4 -->

</div> <!-- container -->